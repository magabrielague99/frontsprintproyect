const base_url = "https://api.sprint3.tk";

let products = [];

var payment_btn = document.getElementById("mercadopago_btn");
payment_btn.addEventListener("click", get_preference_id);

var Hamburguesa_btn = document.getElementById("Hamburguesa_btn");
Hamburguesa_btn.addEventListener("click", get_hamburguesa);

var gaseosa_btn = document.getElementById("gaseosa_btn");
gaseosa_btn.addEventListener("click", get_gaseosa);

function get_gaseosa(e) {
  // paso 1. Preparar el pago (ir al backend y obtener un preference_id)
  // paso 2. Crear un botón que abre la ventana de MercadoPago.
  e.preventDefault();
  console.log("click");
  /* Get all inputs from the form */
  const cantidad = document.getElementById("CantidadGaseosa").value;
  console.log(cantidad);
  const hamburgues = {
    title: "Gaseosa",
    unit_price: 2000,
    quantity: parseInt(cantidad),
  };

  products.push(hamburgues);
}

function get_hamburguesa(e) {
  // paso 1. Preparar el pago (ir al backend y obtener un preference_id)
  // paso 2. Crear un botón que abre la ventana de MercadoPago.
  e.preventDefault();
  console.log("click");
  /* Get all inputs from the form */
  const cantidad = document.getElementById("CantidadHamburguesa").value;
  const hamburgues = {
    title: "Hamburguesa",
    unit_price: 8000,
    quantity: parseInt(cantidad),
  };
  products.push(hamburgues);
}

function get_preference_id(e) {
  // paso 1. Preparar el pago (ir al backend y obtener un preference_id)
  // paso 2. Crear un botón que abre la ventana de MercadoPago.
  e.preventDefault();
  console.log("click");

  const payment_url = `${base_url}/mercadopago/pago`;
  let MERCADOPAGO_PUBLIC_KEY = "APP_USR-b7c99d18-c076-4a74-852d-386429cc55da";

  if (products.length > 0) {
    const data = {
      amount: 1234,
      items: products,
    };

    fetch(payment_url, {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify(data),
    })
      .then((response) => response.json())
      .then((data) => {
        console.log(data);
        const preference_id = data.preference_id;
        const url = data.url;
        const redirect = true; // change this to have different views

        if (redirect) {
          // use the URL if you want to redirect
          console.log(`Redireccionar a la url: ${url}`);
          window.location.href = url;
        } else {
          // Use preference_id to show a modal
          const mp = new MercadoPago(MERCADOPAGO_PUBLIC_KEY, {
            locale: "es-AR",
          });

          // Inicializa el checkout
          mp.checkout({
            preference: {
              id: preference_id,
            },
            render: {
              container: ".cho-container", // Indica el nombre de la clase donde se mostrará el botón de pago
              label: "Pagar", // Cambia el texto del botón de pago (opcional)
            },
          });
        }
      });
  } else {
    console.log("fallo");
    return;
  }
}

var payment_btn2 = document.getElementById("paypal_btn");
payment_btn2.addEventListener("click", get_preference_paypal);

function get_preference_paypal(e) {
  // paso 1. Preparar el pago (ir al backend y obtener un preference_id)
  // paso 2. Crear un botón que abre la ventana de MercadoPago.
  e.preventDefault();
  console.log("click");

  const payment_url = `${base_url}/paypal/pago`;

  let valueProducts = 0;
  products.map((p) => (valueProducts += p.unit_price * p.quantity));
  const data = { currency_code: "USD", value: valueProducts };

  fetch(payment_url, {
    method: "POST",
    headers: { "Content-Type": "application/json" },
    body: JSON.stringify(data),
  })
    .then((response) => response.json())
    .then((data) => {
      console.log(data);
      //debugger
      const url = data.href;
      window.location.href = url;
    });
}
